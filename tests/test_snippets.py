import pytest
from pykairos.snippets import *


def test_get_cafile():
    """ Check that cafile exists and is not empty (it should never be empty)"""
    cafile = get_cafile()
    with open(cafile, "r") as _file:
        assert len(_file.read())


def test_add_certificate():
    """ 
    We do not want to check this function as we do not want to add certificates. 
    Instead we will juste check that connection can be open in append mode.
    """
    add_ca("")


@pytest.mark.parametrize("value,expected", [
  (u"aaa", True),
  (r"aaa", True),
  ("aaa", True),
  (u'哈哈', True),
  (12, False),
  (object, False)
])
def test_is_string(value, expected):
    assert is_string(value) == expected


@pytest.mark.parametrize("path", [
  "/",
  "./",
  "..",
  "/home",
])
def test_is_valid_path(path):
    assert is_valid_path(path)

@pytest.mark.parametrize("path,expected", [
  ("/", "/"),
  ("./", os.getcwd()),
  ("..", str(Path(os.getcwd()).parent)),
  ("/home", "/home")
])
def test_get_posix_dirpath(path, expected):
    assert get_posix_dirpath(path) == expected 


@pytest.mark.parametrize("path,error", [
  ("/ser/fgrg/", DirectoryNotFoundError),
  ('/etc/hosts', FileInsteadOfDirectoryError)
])
def test_get_posix_dirpath_raise_error(path, error):
    with pytest.raises(error):
        get_posix_dirpath(path)


@pytest.mark.parametrize("path,error", [
  ("/tmp", DirectoryInsteadOfFileError),
  ('/ndcefirebg', FileNotFoundError)
])
def test_get_posix_filepath_raise_error(path, error):
    with pytest.raises(error):
        get_posix_filepath(path)


@check_dir
def dummy_func(path):
  """ Dummy function to test check_dir decorator """
  return path

@pytest.mark.parametrize("path,expected", [
  ("/", "/"),
  ("./", os.getcwd()),
  ("..", str(Path(os.getcwd()).parent)),
  ("/home", "/home")
])
def test_check_dir(path, expected):
    assert dummy_func(path) == expected


@pytest.mark.parametrize("path,error", [
  ("/etc/hosts", FileInsteadOfDirectoryError),
  ('/ndcefirebg', DirectoryNotFoundError)
])
def test_check_dir_raise_error(path, error):
    with pytest.raises(error):
        dummy_func(path)


@pytest.mark.parametrize("string,expect", [
  ("True", True),
  ("yes", True),
  ("y", True),
  ("YES", True),
  ("TRUE", True),
  ("ON", True),
  ("On", True),
  ("1", True),
  ("Off", False),
  ("nforing", False)
])
def test_strtobool(string, expect):
    assert strtobool(string) == expect