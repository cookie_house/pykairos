import os
from pykairos import *
from pykairos.snippets import strtobool
import pytest


@pytest.fixture
def kairosdb_con():
	KAIROSDB_HOST=os.environ.get("KAIROSDB_HOST", "kairosdb")
	KAIROSDB_PORT=os.environ.get("KAIROSDB_PORT", "8080")
	KAIROSDB_SSL=os.environ.get("KAIROSDB_SSL", "False")
	opts = {
		"host": KAIROSDB_HOST,
		"port": int(KAIROSDB_PORT),
		"ssl": strtobool(KAIROSDB_SSL)
	}
	return KairosClient(**opts)


@pytest.mark.parametrize("endpoint", [
	"query",
	"query_tags",
	"datapoints",
	"delete_datapoints",
	"delete_metric",
	"health_check",
	"health_status",
	"version",
	"metricnames",
	"tagnames",
	"metadata"
])
def test_kairosdb_urls(kairosdb_con, endpoint):
	assert kairosdb_con.__url__(endpoint)
	

def test_init_kairosdbconnector(kairosdb_con):
	assert kairosdb_con.version


def test_list_metrics(kairosdb_con):
	assert len(kairosdb_con.list_metrics(include_kairos_metrics=True))


def test_is_healthy(kairosdb_con):
	assert kairosdb_con.is_healthy()


def test_get_health_status(kairosdb_con):
	health_status = kairosdb_con.get_health_status() 
	assert health_status[0] == "JVM-Thread-Deadlock: OK"
	assert health_status[1] == "Datastore-Query: OK"


@pytest.mark.parametrize("service,service_key,key,value", [
  ("test_service", "a", "temp", 5),
  ("test_service", "a", "val", 10)
])
def test_add_metadata_value(kairosdb_con, service, service_key, key, value):
	status_code, response = kairosdb_con.add_metadata_value(service, service_key, key, value)
	assert status_code == 204


@pytest.mark.parametrize("service,service_key,key,value", [
  ("test_service/", "a", "temp", 5),
  ("test_service", "//a", "temp", 5),
  ("test_service", "//a", "te/mp", 5)
])
def test_add_wrong_metadata_value(kairosdb_con, service, service_key, key, value):
	with pytest.raises(ValueError):
		kairosdb_con.add_metadata_value(service, service_key, key, value)


@pytest.mark.parametrize("service,service_key,key,expected", [
  ("test_service", "a", "temp", "5")
])
def test_get_metadata_value(kairosdb_con, service, service_key, key, expected):
	value = kairosdb_con.get_metadata_value(service, service_key, key)
	assert value == expected


@pytest.mark.parametrize("service,service_key,key", [
  ("test_service", "a", "unknown")
])
def test_get_wrong_metadata_value(kairosdb_con, service, service_key, key):
	with pytest.raises(errors.KairosDBError):
		value = kairosdb_con.get_metadata_value(service, service_key, key)


@pytest.mark.parametrize("service,service_key,key", [
  ("test_service", "a", "temp"),
  ("test_service", "a", "val")
])
def test_delete_metadata_service_key_key(kairosdb_con, service, service_key, key):
	status_code, response = kairosdb_con.delete_metadata_service_key_key(service, service_key, key)
	assert status_code == 204

	# Check if it was really deleted even if returned the right status code
	with pytest.raises(errors.KairosDBError):
		value = kairosdb_con.get_metadata_value(service, service_key, key)