from pykairos.client import iterable_to_pandas
import pytest


@pytest.mark.parametrize("iterator", [
  [
    [
      "dummy_metric1",
      [
        [1548946894859, 1.],
        [1548946894860, 2.],
      ]
    ],
    [
      "dummy_metric2",
      [
        [1548946894859, 11.],
        [1548946894860, 12.],
      ]
    ]
  ]
])
def test_iterable_to_pandas(iterator):
    df = iterable_to_pandas(iterator)
    assert df.shape[0] == 2
    assert df.shape[1] == 2
    assert all(df.columns == ["dummy_metric1", "dummy_metric2"])

