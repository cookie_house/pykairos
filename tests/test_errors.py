import pytest
from pykairos.errors import *

# Ensure that custom errors can be raised and accepts messages like any Python error
@pytest.mark.parametrize("error", [
    UnitNotKnownError,
    DateManipulationError,
    PrecisionNotKnownError,
    FileInsteadOfDirectoryError,
    DirectoryInsteadOfFileError,
    DirectoryNotFoundError
])
def test_error_with_message(error):
    with pytest.raises(Exception):
        _err = error("Error message")
        raise _err
    assert _err.label
    assert _err.args[0] == "Error message"
