import pytest
from pykairos.dates import *


@check_unit
def dummy_func(unit):
  """ Dummy function accepting a unit argument """
  return unit

@check_precision
def dummy_func2(unit):
  """ Dummy function accepting a unit argument """
  return unit

@pytest.mark.parametrize("unit", [
  "milliseconds",
  "seconds",
  "minutes",
  "hours",
  "days",
  "weeks",
  "months",
  "years"
])
def test_check_unit_is_valid(unit):
    assert dummy_func(unit) == unit


@pytest.mark.parametrize("unit", [
  "millieeseconds",
  2,
  "minutfees",
  object
])
def test_check_unit_raise_error(unit):
    with pytest.raises(UnitNotKnownError):
        dummy_func(unit)


@pytest.mark.parametrize("unit", [
  "s",
  "seconds",
  "milliseconds",
  "ms",
  "microseconds",
  "us"
])
def test_check_divisor_is_valid(unit):
    assert dummy_func2(unit) == unit

@pytest.mark.parametrize("unit", [
  "weeks",
  "w",
  "months",
  "years",
  object,
  "aaa"
])
def test_check_divisor_is_valid(unit):
    with pytest.raises(PrecisionNotKnownError):
        dummy_func2(unit)


@pytest.mark.parametrize("python_datetime,unit,expected", [
   (datetime.datetime(2018,1,1,0,0,10), "s", 1514761210),
   (datetime.datetime(2018,1,1,0,0,10,6), "s", 1514761210),
   (datetime.datetime(2018,1,1,0,0,10,600000), "s", 1514761210),
   (datetime.datetime(2018,1,1,0,0,10,6000), "ms", 1514761210006),
   (datetime.datetime(2018,1,1,0,0,10,6000), "us", 1514761210006000),
])
def test_datetime_to_timestamp(python_datetime, unit, expected):
    assert datetime_to_timestamp(python_datetime, unit=unit) == expected

@pytest.mark.parametrize("python_datetime,unit,expected", [
   (datetime.datetime(2018,1,1,0,0,10), "s", 1514761210),
   (datetime.datetime(2018,1,1,0,0,10,6), "seconds", 1514761210),
   (datetime.datetime(2018,1,1,0,0,10,600000), "s", 1514761210),
   (datetime.datetime(2018,1,1,0,0,10,6000), "ms", 1514761210006),
   (datetime.datetime(2018,1,1,0,0,10,6000), "milliseconds", 1514761210006),
   (datetime.datetime(2018,1,1,0,0,10,6000), "us", 1514761210006000),
   (datetime.datetime(2018,1,1,0,0,10,6000), "microseconds", 1514761210006000)
])
def test_datetime_to_timestamp_factory(python_datetime, unit, expected):
    func = datetime_to_timestamp_factory(unit)
    assert func(python_datetime) == expected


@pytest.mark.parametrize("timestamp,unit,expected", [
  (1514761210, "s", datetime.datetime(2018,1,1,0,0,10)),
  (1514761210, "seconds", datetime.datetime(2018,1,1,0,0,10)),
  (1514761210006, "ms", datetime.datetime(2018,1,1,0,0,10,6000)),
  (1514761210006000, "us", datetime.datetime(2018,1,1,0,0,10,6000)),
])
def test_timestamp_to_datetime(timestamp, unit, expected):
    assert timestamp_to_datetime(timestamp, unit) == expected

@pytest.mark.parametrize("timestamp,unit,expected", [
  (1514761210, "s", datetime.datetime(2018,1,1,0,0,10)),
  (1514761210, "seconds", datetime.datetime(2018,1,1,0,0,10)),
  (1514761210006, "ms", datetime.datetime(2018,1,1,0,0,10,6000)),
  (1514761210006000, "us", datetime.datetime(2018,1,1,0,0,10,6000))
])
def test_timestamp_to_datetime_factory(timestamp, unit, expected):
    func = timestamp_to_datetime_factory(unit)
    assert func(timestamp) == expected


@pytest.mark.parametrize("str_datetime,expected", [
  ("2018-01-01 01", datetime.datetime(2018,1,1,1,0,0)),
  ("2018-01-01 01:00:00", datetime.datetime(2018,1,1,1,0,0)),
  ("2018-01-01T01:00:00", datetime.datetime(2018,1,1,1,0,0)),
  ("2018-01-01T01:00:00Z", datetime.datetime(2018,1,1,1,0,0)),
  ("2018-01-01T01:00:00.000Z", datetime.datetime(2018,1,1,1,0,0)),
  ("2018-01-01T01:00:00.000000Z", datetime.datetime(2018,1,1,1,0,0)),
  ("20180101010000", datetime.datetime(2018,1,1,1,0,0)),
  ("20180101 010000", datetime.datetime(2018,1,1,1,0,0))
])
def test_parse_any_datetime(str_datetime, expected):
    assert parse_any_datetime(str_datetime) == expected


@pytest.mark.parametrize("str_datetime,format", [
  ("2018-01-01T01:01:01", "%Y-%m-%dT%H:%M:%S"),
  ("2018-01-01 01:01:01", "%Y-%m-%d %H:%M:%S"),
  ("2018-01-01_01:01:01", "%Y-%m-%d_%H:%M:%S"),
])
def test_parse_datetime(str_datetime, format):
    parse_datetime(str_datetime, format)
