import pytest
from pykairos.json import *


@pytest.mark.parametrize("obj", [
  {
    "date": datetime.datetime(2018,1,1),
    "int32": numpy.int32(1),
    "int64": numpy.int64(2),
    "float32": numpy.float32(1),
    "float64": numpy.float64(2),
    "ndarray": numpy.array([1, 2 ,3])
  }
])
def test_tostrjson(obj):
    assert type(to_strjson(obj)) == str
    assert json.loads(to_strjson(obj))
