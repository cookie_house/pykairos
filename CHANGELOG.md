# Changelog

* Version 1.0.0: 
    - Query metric names
    - Insert single datapoint
    - Insert several datapoints
    - Query metrics
    - Query using aggregators
    - Query metadata keys
    - Add/Update metadata keys