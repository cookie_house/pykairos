"""
------------------------------------------------
@author: Guillaume Charbonnier (Capgemini, 2018)
------------------------------------------------

"""
from functools import wraps
from inspect import getfullargspec

import datetime
import dateutil
from dateutil.parser import parse as date_parser

from .errors import UnitNotKnownError, PrecisionNotKnownError

VALID_TIME_UNITS = [
  "milliseconds",
  "seconds",
  "minutes",
  "hours",
  "days",
  "weeks",
  "months",
  "years"
]

DIVISOR_PER_UNIT = {
  "s": 1,
  "seconds": 1,
  "milliseconds": 1e3,
  "ms": 1e3,
  "microseconds": 1e6,
  "us": 1e6
}

def check_unit(func):
    """
    Decorator used to check the input value of parameter unit in several functions.
    Authorized valued are items of VALID_TIME_UNITS dict.
    Based on https://stackoverflow.com/questions/40061956/python-decorator-that-returns-a-function-with-one-or-more-arguments-replaced
    """
    unitpos = getfullargspec(func).args.index('unit')
    @wraps(func)
    def wrapper(*args, **kwargs):
        if len(args) > unitpos:
            args = list(args)
            if args[unitpos] not in VALID_TIME_UNITS:
                raise UnitNotKnownError('Possible values for parameter \
                unit are: {0}. Given value was : {1}'.format(
                    VALID_TIME_UNITS,
                    args[unitpos]
                    )
                )
        else:
            # By default millisecond is used
            kwargs['unit'] = kwargs.get("unit","ms")
        return func(*args, **kwargs)
    return wrapper


def check_precision(func):
    """
    Decorator used to check the input value of parameter unit in several functions.
    Authorized valued are items of DIVISOR_PER_UNIT dict.
    Based on https://stackoverflow.com/questions/40061956/python-decorator-that-returns-a-function-with-one-or-more-arguments-replaced
    """
    unitpos = getfullargspec(func).args.index('unit')
    @wraps(func)
    def wrapper(*args, **kwargs):
        if len(args) > unitpos:
            args = list(args)
            if args[unitpos] not in DIVISOR_PER_UNIT:
                raise PrecisionNotKnownError('Possible values for parameter \
                unit are: {0}. Given value was : {1}'.format(
                    tuple(DIVISOR_PER_UNIT.keys()),
                    args[unitpos]
                    )
                )
        else:
            # By default millisecond is used
            kwargs['unit'] = kwargs.get("unit","ms")
        return func(*args, **kwargs)
    return wrapper


def datetime_to_timestamp(python_datetime, unit="ms"):
    """
    Convert a python datetime.datetime object into a timestamp
    (in milliseconds by default)

    Parameters:
    ------------
        python_datetime (datetime.datetime): A datetime object
        unit (str): A valid unit from DIVISOR_PER_UNIT keys.

    Returns:
    ------------
        timestamp (int): A timestamp with desired precision. 
    """
    return int(python_datetime.timestamp() * DIVISOR_PER_UNIT[unit])


def timestamp_to_datetime(timestamp, unit="ms"):
    """
    Convert a timestamp (in milliseconds by default) into a 
    datetime.datetime object.

    Parameters:
    ------------
        timestamp (int): An integer timestamp
        unit (str): Precision of given timestamp.

    Returns:
    -----------
        datetime (datetime.datetime): A datetime.datetime object
    """
    return datetime.datetime.fromtimestamp(timestamp/DIVISOR_PER_UNIT[unit])


@check_precision
def datetime_to_timestamp_factory(unit):
    """
    This function takes as argument one of the unit available
    from DIVISOR_PER_UNIT keys and returns a function that converts datetime 
    into timestamp with the desired unit.

    Parameters:
    -----------
      unit (str): Either 'seconds', 's', 'milliseconds', 'ms', 'microseconds', 'us'

    Returns:
    -----------
      timestamp_factory (func): A function that converts datetime to desired timestamps.
    """
    def to_timestamp(python_datetime):
        return datetime_to_timestamp(python_datetime, unit)

    return to_timestamp


@check_precision
def timestamp_to_datetime_factory(unit):
    """
    Similar to `datetime_to_timestamp_factory` but returns a function to
    converts from timestamp to python datetime objects.

    Parameters:
    ------------
        unit (str): Precision of timestamps that returned function should convert

    Returns:
    ------------
        datetime_factory (func): A function that converts timestamp to datetime objects.
    """
    def to_datetime(timestamp):
        return timestamp_to_datetime(timestamp, unit)
    return to_datetime


def parse_any_datetime(str_datetime):
    """
    This function uses the `dateutils.parser.parse` function to try 
    to parse any given string as python datetime.
    Timezone are ignored.
    """
    return date_parser(str_datetime, ignoretz=True)


def parse_datetime(str_datetime, format="%Y-%m-%dT%H:%M:%S"):
    """
    Parse a dateitme object from a given string and given format.
    """
    return datetime.datetime.strptime(str_datetime, format)
