"""
------------------------------------------------
@author: Guillaume Charbonnier (Capgemini, 2018)
------------------------------------------------

"""
class DateManipulationError(Exception):
    def __init__(self,*args,**kwargs):
        super().__init__(*args, **kwargs)
        self.label = "An error occured during date manipulation"


class UnitNotKnownError(DateManipulationError):
    def __init__(self,*args,**kwargs):
        super().__init__(*args, **kwargs)
        self.label = "Cannot find given unit in unit mapping table"


class PrecisionNotKnownError(DateManipulationError):
    def __init__(self,*args,**kwargs):
        super().__init__(*args, **kwargs)
        self.label = "Cannot find given precision in precision mapping table"


class FileInsteadOfDirectoryError(FileExistsError):
    def __init__(self,*args,**kwargs):
        super().__init__(*args, **kwargs)
        self.label = "A file was found when looking for a directory"


class DirectoryInsteadOfFileError(FileExistsError):
    def __init__(self,*args,**kwargs):
        super().__init__(*args, **kwargs)
        self.label = "A directory was found when looking for a file"


class DirectoryNotFoundError(FileNotFoundError):
    def __init__(self,*args,**kwargs):
        super().__init__(*args, **kwargs)
        self.label = "Cannot find directory"


class DatabaseError(Exception):
    def __init__(self,*args,**kwargs):
        super().__init__(*args, **kwargs)
        self.label = "Database error"


class DatabaseNotKnownError(DatabaseError):
    def __init__(self,*args,**kwargs):
        super().__init__(*args, **kwargs)
        self.label = "Trying to connect to unknown kind of database"


class MissingNonOptionalParameterError(DatabaseError):
    def __init__(self,*args,**kwargs):
        super().__init__(*args, **kwargs)
        self.label = "Cannot connect to database because non optional parameter is missing"


class KairosDBError(DatabaseError):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.label = "KairosDB error"


class MetricNotFoundError(KairosDBError):
    def __init__(self,*args,**kwargs):
        super().__init__(*args, **kwargs)
        self.label = "Cannot find such metric in KairosDB database"


class EndpointDoesNotExist(KairosDBError):
    def __init__(self,*args,**kwargs):
        super().__init__(*args, **kwargs)
        self.label = "Endpoint is not a valid KairosDB Rest API endpoint"

