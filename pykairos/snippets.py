"""
------------------------------------------------
@author: Guillaume Charbonnier (Capgemini, 2018)
------------------------------------------------

"""
import os
import fnmatch
from glob import glob
from functools import wraps
from inspect import getfullargspec

import certifi
import six
from pathlib import Path

from .errors import (
  FileInsteadOfDirectoryError,
  DirectoryInsteadOfFileError,
  DirectoryNotFoundError
)


def get_cafile():
    """ Return path to the Certificate Authority file containing allowed certificates """
    return certifi.where()


def add_ca(certificate):
    """ Add a certificate as string to allowed certificates. Returns nothing """
    cafile = get_cafile()
    with open(cafile, "a") as allowed_certificates:
        allowed_certificates.write(certificate)


def is_string(value):
    """ 
    Return True if given value is a string, else return False
    
    Parameters:
    -----------
        value (object): Any python object
      
    Returns:
    -----------
        is_string (bool): A boolean, True if given value is a string, else False
    """
    return isinstance(value, six.string_types)


def is_valid_path(path):
    """ Return True if given path exists, else return False """
    path = Path(path)
    return path.exists()


def get_posix_filepath(path):
    """ Return valid posix path from absolute or relative file path """
    path = Path(path)
    if path.is_dir():
        raise DirectoryInsteadOfFileError("Given path is a directory: {0}".format(path))
    if not path.is_file():
        raise FileNotFoundError("Given file does not exist: {0}".format(path))
    return str(path.resolve())


def get_posix_dirpath(path):
    """ Return valid posix path from absolute or relative directory path """
    path = Path(path)
    if path.is_file():
        raise FileInsteadOfDirectoryError("Given path is a file: {0}".format(path))
    if not path.is_dir():
        raise DirectoryNotFoundError("Given directory does not exist: {0}".format(path))
    return str(path.resolve())


def check_dir(func):
    """ 
    Function to use as decorator.
    Ensure keyword argument or positional argument 'path' is a valid directory path.
    
    Based on https://stackoverflow.com/questions/40061956/python-decorator-that-returns-a-function-with-one-or-more-arguments-replaced
    """
    arg="path"
    argpos = getfullargspec(func).args.index(arg)
    @wraps(func)
    def wrapper(*args, **kwargs):
        if len(args) > argpos:
            args = list(args)
            args[argpos] = get_posix_dirpath(args[argpos])
        else:
            kwargs[arg] = get_posix_dirpath(kwargs.get(arg,"./"))
        return func(*args, **kwargs)
    return wrapper


def check_file(func):
    """ 
    Function to use as decorator.
    Ensure keyword argument or positional argument 'path' is a valid filepath
    See `pymonsoon2.snippets.read_filter()` for an example usage.
    """
    arg="path"
    argpos = getfullargspec(func).args.index(arg)
    @wraps(func)
    def wrapper(*args, **kwargs):
        if len(args) > argpos:
            args = list(args)
            args[argpos] = get_posix_filepath(args[argpos])
        else:
            kwargs[arg] = get_posix_filepath(kwargs[arg])
        return func(*args, **kwargs)
    return wrapper


@check_file
def read_filter(path):
    with open(path, "r") as f:
        metrics = [line.strip() for line in f.readlines()]
    return metrics


@check_dir
def recursive_yield_filepaths(path, pattern="*"):
    """Search recursively for files matching a specified pattern.

    Adapted from http://stackoverflow.com/questions/2186525/use-a-glob-to-find-files-recursively-in-python
    """
    for root, dirnames, filenames in os.walk(path):
        for filename in fnmatch.filter(filenames, pattern):
            # yield Path(os.path.join(root, filename)).as_posix()
            yield get_posix_filepath(os.path.join(root, filename))


@check_dir
def yield_filepaths(path, pattern="*"):
    """
    Search for files matching a specified pattern. 
    It uses the same code as the recursive function but breaks on first directory.

    Adapted from http://stackoverflow.com/questions/2186525/use-a-glob-to-find-files-recursively-in-python
    """
    for root, dirnames, filenames in os.walk(path):
        for filename in fnmatch.filter(filenames, pattern):
            # yield Path(os.path.join(root, filename)).as_posix()
            yield get_posix_filepath(os.path.join(root, filename))
        break


def strtobool(string):
    """
    Converts a string to boolean value.
    Given string is first converted to lowercase.
    If lowercase string is equal to 'true', 't', 'y', 'yes', 'on', '1'
    """
    return string.lower() in ["true", "t", "y", "yes", "on", "1"]