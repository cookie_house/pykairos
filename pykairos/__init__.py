"""
------------------------------------------------
@author: Guillaume Charbonnier (Capgemini, 2018)
------------------------------------------------

"""
__version__ = "1.0.0"

from .client import KairosClient