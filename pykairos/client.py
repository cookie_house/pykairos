"""
------------------------------------------------
@author: Guillaume Charbonnier (Capgemini, 2018)
------------------------------------------------

"""
import requests
import numpy as np
import pandas

from .json import to_strjson, json
from .snippets import is_string
from .dates import (
    timestamp_to_datetime,
    datetime_to_timestamp,
    parse_any_datetime
)
from .errors import (
    KairosDBError,
    MetricNotFoundError,
    EndpointDoesNotExist
)


def __iterate_on_query_response__(response, return_tags=False):
    """
    Iterate over elements of "queries" field returned from KairosDB REST API.

    Returns:
    ---------
        metric_name, values (str, list)
    """
    if not return_tags:
        for returned_query in response["queries"]:
            yield returned_query["results"][0]["name"], returned_query["results"][0]["values"]
    else:
        for returned_query in response["queries"]:
            yield returned_query["results"][0]["name"], returned_query["results"][0]["tags"]


def iterable_to_pandas(iterator, join="outer", keep_duplicated="last"):
    """
    Transforms an iterator object into a pandas DataFrame.
    
    Parameters:
    ------------
        iterator    (iterable): An iterable with elements of size 2. Each element should be a tuple (name, values).
                                Name being a string and values being an iterable of [timestamp, value]
        join        (str): Either 'outer' or 'inner'. 
                           See https://pandas.pydata.org/pandas-docs/version/0.23.4/generated/pandas.concat.html
                           for more information regarding this option.
        keep_duplicated (str): Which duplicated rows to keep. 
                               See https://pandas.pydata.org/pandas-docs/stable/reference/api/pandas.Index.duplicated.html
                               for more information regarding this option.

    Returns:
    ------------
        df (pandas.DataFrame): The queried dataset as a pandas DataFrame
    """
    dfs = list()
    for name, values in iterator:
        # Continue to next metric results if values is empty
        if not len(values):
            continue
        # Construct a DataFrame with values and set colnames using name of the metric
        # Keep in ming that each element of values looks like [timestamp, value]
        new_df = pandas.DataFrame(values, columns=["timestamp", name])
        # Use timestamp column as index and drop the column
        new_df.set_index("timestamp", drop=True, inplace=True)
        # If keep_duplicated is not False, I.E, last or first, we keep only the last or fist duplicated rows
        if keep_duplicated:
            new_df = new_df.loc[~new_df.index.duplicated(keep=keep_duplicated)]
        # We add the newly created dataframe to the list of dataframe
        dfs.append(new_df)
    if len(dfs)>1:
        # If we got more than 1 dataframe with merge the dataframe using the timestamp index (default behaviour)
        DF = pandas.concat(dfs, axis=1, join=join)
        # We transform the index into datetime objects
        DF.index = DF.index.map(timestamp_to_datetime)
        # We return the DF
        return DF
    elif len(dfs):
        # If there is exactly one DF we return it after transforming the index to datetime objects
        DF = dfs[0]
        DF.index = DF.index.map(timestamp_to_datetime)
        return DF
    else:
        # If no DF were returned, return an empty dataframe
        return pandas.DataFrame()


class Tags(dict):
    """
    Tags to use with KairosDB API
    """   
    def __init__(self, **kwargs):
        """
        Create a tag object from keyword arguments
        """
        for key, values in kwargs.items():
            if not isinstance(values, list):
                self[key] = [values]
            else:
                self[key] = values
                
    @classmethod
    def from_dict(cls, _dict):
        if not isinstance(_dict, dict):
            raise TypeError("You must give a dictionary as argument. \
            Given value was {0} and of type {1}".format(_dict, type(_dict)))
        return cls(**_dict)
    
    def to_json(self, indent=0):
        """ Get json serialization of tags """
        return to_strjson(self, indent=indent)
    
    def __repr__(self):
        """ String representation of tags """
        return self.to_json(indent=2)


class Metric(dict):
    """
    Metric to use with KairosDB API
    """
    def __init__(self,
                 name,
                 tags=None,
                 limit=None,
                 aggregators=None,
                 exclude_tags=False,
                 group_by=None,
                 order="asc"):
        # Store name of metric
        self.name = name
        # Store tags as Tags object
        self.tags = tags
        # Store limit property
        self.limit = limit
        # Store aggregators
        self.aggregators = aggregators
    
    @property
    def name(self):
        """ Get name property of the Metric """
        return self.get("name")
    
    @name.setter
    def name(self, value):
        """ Set name property of the Metric """
        if is_string(value):
            self["name"] = value
        else:
            raise TypeError("'name' must be a string.\
                Given value was {0} and of type {1}".format(value, type(value)))
        
    @property
    def tags(self):
        """ Get tags property of the Metric"""
        return self.get("tags")
    
    @tags.setter
    def tags(self, value):
        """ Set tags property of the Metric """
        if value is None:
            return
        if not isinstance(value, Tags):
            value = Tags.from_dict(value)
        self["tags"] = value
        
    @property
    def limit(self):
        """ Get limit property of the Metric """
        return self.get("limit")
        
    @tags.setter
    def limit(self, value):
        """ Set limit property of the Metric """
        if value is None:
            return
        if value >= 0:
            self["limit"] = value
        else:
            raise ValueError("You must set a value equal or greater than zero for 'limit' property. \
                Given value was : {0}".format(value))
            
    @property
    def aggregators(self):
        return self.get("aggregators")
    
    @aggregators.setter
    def aggregators(self, aggregators):
        """ Set the aggregators property """
        if aggregators is None:
            return
        if not isinstance(aggregators, list):
            aggregators = [aggregators]
        is_aggregator = [isinstance(agg, Aggregator) for agg in aggregators]
        if not all(is_aggregator):
            raise ValueError("Aggregators must be of type Aggregator")
        self["aggregators"] = aggregators
        
    def to_json(self, indent=0):
        """ Get json serialization of aggregator """
        return to_strjson(self, indent=indent)
    
    def __repr__(self):
        """ String representation of aggregator """
        return self.to_json(indent=2)


class Aggregator(dict):
    """
    KairosDB can return aggregated queries. In order to do so the user must provide a metric property called "aggregators".
    Available aggregators are listed at : https://kairosdb.github.io/docs/build/html/restapi/Aggregators.html
    List of aggregators:
    
    - Average : 'avg'
    - Standard Deviation : 'dev'
    - Count : 'count'
    - First : 'first'
    - Gaps : 'gaps'
    - Histogram : 'histogram'
    - Last : 'last'
    - Least Squares : 'least_squares'
    - Max : 'max'
    - Min : 'min'
    - Percentile : 'percentile'
    - Sum : 'sum'
    - Diff : 'diff'
    - Divide : 'duv'
    - Rate : 'rate'
    - Sampler : 'sampler'
    - Scale : 'scale'
    - Trim : 'trim'
    - Save as : 'save_as'
    - Filter : 'filter' ### Can be particulary useful if we want to remove negative values for example
    - JS Aggregators :
       - 'js_function'
       - 'js_filter'

    Example of aggregator:
    
        "aggregators": [
            {
              "name": "sum",
              "align_sampling": true,
              "align_start_time": true,
              "sampling": {
                "value": 1,
                "unit": "minutes"
                }
            }
        ]

    This is not really easy to remember and to construct, that's why I decided to create this class.
    """

    def __init__(self, name, sampling=None, align_start_time=False, align_end_time=False, align_sampling=False, **kwargs):
        """

        """
        # Set name of the aggregator
        self.name = name
        # Set sampling property
        if sampling:
            self.sampling = sampling
        # Set align properties
        self.align_start_time = align_start_time
        self.align_end_time = align_end_time
        self.align_sampling = align_sampling
        for key, value in kwargs.items():
            self[key] = value

    @property
    def name(self):
        """ Get the name property of the aggregator """
        return self.get("name")
    
    @name.setter
    def name(self, value):
        """ Set the name property of the aggregator """
        if is_string(value):
            self["name"] = value
        else:
            raise ValueError("'name' property must be a string. Given value was {0} and of type {1}".format(value, type(value)))
    
    @property
    def sampling(self):
        """ Get the sampling property of the aggregator """
        return self.get("sampling")

    @sampling.setter
    def sampling(self, values):
        """ Set the sampling property of the aggregator """
        try:
            value = values[0]
            unit = values[1]
        except IndexError:
            raise ValueError("'sampling' property must be a tuple of length 2. First element is value, second element is unit.\
            Examples: (1, 'minutes'), (1, 'hours')")
        else:
            self["sampling"] = {
                "value": value,
                "unit": unit
            }
            
    @property
    def align_start_time(self):
        """ Get the align_start_time property of the aggregator """
        return self.get("align_start_time", False)
        
    @align_start_time.setter
    def align_start_time(self, align_start_time=False):
        """ 
        Set the align_start_time property of the aggregator. 
        align_start_time, align_end_time and align_sampling are mutually exclusive, I.E,
        you cannot set more than one of those properties to True.
        """
        if align_start_time:
            self["align_start_time"] = True
            self["align_end_time"] = False
            self["align_sampling"] = False
    
    @property
    def align_end_time(self):
        """ Get the align_end_time property of the aggregator """
        return self.get("align_end_time", False)
        
    @align_end_time.setter
    def align_end_time(self, align_end_time=False):
        """ 
        Set the align_end_time property of the aggregator. 
        align_start_time, align_end_time and align_sampling are mutually exclusive, I.E,
        you cannot set more than one of those properties to True.
        """
        if align_end_time:
            self["align_end_time"] = True
            self["align_start_time"] = False
            self["align_sampling"] = False
            
    @property
    def align_sampling(self):
        """ Get the align_sampling property of the aggregator """
        return self.get("align_sampling", False)
        
    @align_sampling.setter
    def align_sampling(self, align_sampling=False):
        """ 
        Set the align_sampling property of the aggregator. 
        align_start_time, align_end_time and align_sampling are mutually exclusive, I.E,
        you cannot set more than one of those properties to True.
        """
        if align_sampling:
            self["align_sampling"] = True
            self["align_start_time"] = False
            self["align_end_time"] = False
            
    def to_json(self, indent=0):
        """ Get json serialization of metric """
        return to_strjson(self, indent=indent)
    
    def __repr__(self):
        """ String representation of metric """
        return self.to_json(indent=2)


class Query(dict):
    """
    A class representing queries for KairosDB RestAPI.
    You must pass either and absolute or a relative start and either an absolute or a relative end to create a KairosQuery instance.

    .. warning::
        For now tags and aggregators apply to all metrics in the query.
        This might change in the future.
    """

    def __init__(self,
                 metrics,
                 start_absolute=None,
                 end_absolute=None,
                 start_relative=None,
                 end_relative=None,
                 tz="Etc/UTC",
                 cache_time=None):
        """
        Returns an instance of KairosQuery class.
        This class allows to construct queries for KairosDB RestAPI.

        Parameters:
            start_absolute (datetime.datetime): The absolute start of the query as a datetime.datetime object.
            end_absolute (datetime.datetime): The absolute end of the query as datetime.datetime object. 
            start_relative (tuple(str, int)): The relative start of the query. It is a tuple of length 2 : 
                                              First element is the value, second element is the unit. 
                                              Examples : (1, "days"), (2,"hours"), (1,"minutes")
            end_relative (tuple(str, int)): The relative end of the query as string. It is a tuple of length 2:
                                            First element is the value, second element is the unit. 
                                            Examples : (1,"days"), (2,"hours"), (3,"weeks")
            tags (dict): Dictionnary of tags constraints.
                         Example:
                         {
                           "tags": {
                             "host": ["foo", "foo2"],
                             "customer": ["bar"]
                           }
                         }
                         In the above example, the query would return datapoints
                         with tag host equal to 'foo' or 'foo2'
                         and tag customer equal to 'bar'.
            limit (int): Maximum number of points to return
            tz (str): A timezone in long time zone format. 
                      Some examples : UTC : Etc/UTC, Paris : Europe/Paris
            aggregators ():
        """
        # Handle metrics
        self.metrics = metrics
        # Handle start_time
        self.start = {
            "start_absolute": start_absolute,
            "start_relative": start_relative
        }
        # Handle end_time
        self.end = {
            "end_absolute": end_absolute,
            "end_relative": end_relative
        }
        # Handle cache_time
        self.cache_time = cache_time
        # Handle timezone
        self.tz = tz

    @property
    def metrics(self):
        """ Get the metrics property of the query"""
        return self.get("metrics")

    @metrics.setter
    def metrics(self, metrics):
        """ Set the metrics property of the query """
        if not isinstance(metrics, list):
            metrics = [metrics]
        is_metric = [isinstance(metric, Metric) for metric in metrics]
        if not all(is_metric):
            raise TypeError("Each metric must be of type Metric.")
        self["metrics"] = metrics

    @property
    def start(self):
        """ Get the start property of the query """
        return self._start

    @start.setter
    def start(self, value):
        """ Set the start property of the query """
        start_absolute = value.get("start_absolute")
        start_relative = value.get("start_relative")
        if start_absolute:
            if is_string(start_absolute):
                start_absolute = parse_any_datetime(start_absolute)
            self["start_absolute"] = datetime_to_timestamp(start_absolute)
            self._start = ("start_absolute", self["start_absolute"])
        elif start_relative:
            try:
                self["start_relative"] = {
                    "value": start_relative[0],
                    "unit": start_relative[1]
                }
            except IndexError:
                raise ValueError("'start_relative' property must be a tuple of length 2: (value, unit). Example: (1, 'hours')")
            else:
                self._start = ("start_relative", self["start_relative"])
        else:
            raise ValueError("You must set at least start_absolute or start_relative property.")


    @property
    def end(self):
        """ Get the end property of the query """
        return self._end

    @end.setter
    def end(self, value):
        """ Set the end property of the query """
        end_absolute = value.get("end_absolute")
        end_relative = value.get("end_relative")
        if end_absolute:
            if is_string(end_absolute):
                end_absolute = parse_any_datetime(end_absolute)
            self["end_absolute"] = datetime_to_timestamp(end_absolute)
            self._end = ("end_absolute", self["end_absolute"])
        elif end_relative:
            try:
                self["end_relative"] = {
                    "value": end_relative[0],
                    "unit": end_relative[1]
                }
            except IndexError:
                raise ValueError("'end_relative' property must be a tuple of length 2: (value, unit). Example: (1, 'hours')")
            else:
                self._end = ("end_relative", self["end_relative"])  
                  
    @property
    def tz(self):
        """ Get the timezone property of the query """
        return self.get("time_zone")
    
    @tz.setter
    def tz(self, value):
        """ Set the timezone property of the query """
        self["time_zone"] = value
        
    @property
    def cache_time(self):
        """ Get the cache_time property of the query """
        return self.get("cache_time")
    
    @cache_time.setter
    def cache_time(self, value):
        """ Set the cache_time property of the query """
        if value:
            self["cache_time"] = value
        
    def to_json(self, indent=0):
        """ Get json serialization of query """
        return to_strjson(self, indent=indent)
    
    def __repr__(self):
        """ String representation of query """
        return self.to_json(indent=2)


class Points(dict):
    """
    Message to insert datapoints into KairosDB
    """
    def __init__(self, name, tags, datapoints=None, timestamp=None, value=None, ttl=None):
        """
        name (str): Name of the metric that should hold datapoints or point
        tags (dict): A dictionary that will becasted to Tags object
        datapoints (array-like): An array of points. Each element is a list of length 2: [timestamp, value].
                                 If points is not None then timestamp and value options are ignored.
        timestamp (int): Timestamp of the point to insert. Ignored if Points is not None.
        value (numeric): Value of the point to insert. Ignored if Points is not None.
        ttl (int): Sets the Cassandra ttl for the data points.
        """
        self.name = name
        self.tags = tags
        self.datapoints = datapoints
        self.timestamp = timestamp
        self.value = value
        self.ttl = ttl
        
    @property
    def name(self):
        """ Get the name property of the Points """
        return self.get("name")
    
    @name.setter
    def name(self, name):
        """ Set the name property of the points """
        self["name"] = name
        
    @property
    def tags(self):
        """ Get the tags property of the Points """
        return self.get("tags")
    
    @tags.setter
    def tags(self, _dict):
        """ Set the tags property of the points """
        self["tags"] = _dict
        
    @property
    def datapoints(self):
        """ Get the datapoints property of the points """
        return self.get("datapoints", [])
    
    @datapoints.setter
    def datapoints(self, array_like):
        """ Set the datapoints property of the Points """
        if array_like is None:
            return
        array = np.array(array_like)
        if not array.shape[1] == 2:
            raise ValueError("array_like argument must be an iterable of list of two elements: \
                             [ [timestamp1, value1], ... , [timestampN, valueN] ]")
        else:
            self["datapoints"] = array_like
            
    @property
    def timestamp(self):
        """ Get the timestamp property """
        return self.get("timestamp")
    
    
    @timestamp.setter
    def timestamp(self, value):
        """ Set the timestamp property """
        if value is not None:
            try:
                timestamp_to_datetime(value)
            except ValueError:
                raise ValueError("Given timestamp should be a valid milliseconds timestamp. Given value was {0}".format(value))
            self["timestamp"] = value
            
    @property
    def value(self):
        """ Get the value property """
        return self.get("value")
    
    @value.setter
    def value(self, _value):
        """ Set the value property """
        if _value is not None:
            self["value"] = _value

            
    @property
    def ttl(self):
        """ Get the ttl property """
        return self.get("ttl")
    
    @ttl.setter
    def ttl(self, value):
        """ Set the ttl property """
        if value is not None:
            try:
                value = int(value)
            except (TypeError, ValueError, AttributeError):
                raise TypeError("ttl must be an integer")
            else:
                self["ttl"] = value
       
    def to_json(self, indent=0):
        """ Get json serialization of query """
        return to_strjson(self, indent=indent)
    
    def __repr__(self):
        """ String representation of query """
        return self.to_json(indent=2)


class Insert(list):
    """
    Class representing messages to send in order to add datapoints using KairosDB Rest API
    """
    def __init__(self, points):
        """
        This class is quite simple and accepts a list of Points objects or a list of JSON serializable python dictionaries
        """
        if not isinstance(points, list):
            points = [points]
        self.extend(points)
        
    def to_json(self, indent=0):
        """ Get json serialization of query """
        return to_strjson(self, indent=indent)

    def __repr__(self):
        """ String representation of query """
        return self.to_json(indent=2)
    
    @classmethod
    def from_pandas(cls, df, tag_columns=None, tags=None, timestamp_field="index", to_timestamp=None, ignore_columns=None):
        """
        Create an Insert message from a pandas DataFrame
        """
        # Use a copy instead of the input dataframe
        df = df.copy()

        # Drop unwanted columns
        if ignore_columns is not None:
            df.drop(columns=ignore_columns, inplace=True)

        # If no tag is set then raise an error
        if tags is None and tag_columns is None:
            raise ValueError("You must set at least one column to use as tag with `tag_columns` option or \
            specify tag for all dataframe with `tags` options.")
            
        if tag_columns is None:
            _tag_columns = []
        else:
            _tag_columns = tag_columns
        if tags is None:
            _tags = {}
        else:
            _tags = tags
            
        if to_timestamp is None:
            to_timestamp = lambda x: x
            
        if timestamp_field == "index":
            metric_columns = list(set(df.columns) - set(_tag_columns))
            get_timestamp = lambda x: [to_timestamp(timestamp_value) for timestamp_value in x.index.tolist()]
        else:
            metric_columns = list(set(df.columns) - set(_tag_columns) - set([timestamp_field]))
            get_timestamp = lambda x: [to_timestamp(timestamp_value) for timestamp_value in x[timestamp_field].tolist()]

        # Group by in order to have other tags
        if tag_columns is not None:
            # We want a list of columns, so if given option is a string we create a list
            if is_string(tag_columns):
                _tag_columns = [_tag_columns]

            # Group by the df
            groupped_df = df.groupby(_tag_columns)
            insert= cls([])

            # Process each group
            for group, sub_df in groupped_df:
                if len(_tag_columns) == 1:
                    tags = {tag_columns[0]: group}
                else:
                    tags = dict(zip(tag_columns, group))
                # Add global tags
                tags.update(_tags)
                # Get datapoints 
                _points = [
                    {
                        "name": metric,
                        "tags": tags,
                        "datapoints": list(zip(get_timestamp(sub_df), sub_df[metric].tolist()))
                    }
                    for metric in metric_columns
                ]
                points = [Points(**points_as_dict) for points_as_dict in _points]
                insert.extend(points)
            return insert
        
        else:
            _points = [
                {
                    "name": metric,
                    "tags": _tags,
                    "datapoints": list(zip(get_timestamp(df), df[metric].tolist()))
                }
                for metric in metric_columns
            ]
            insert = Insert(_points)
            return insert


class KairosClient:
    """
    A connection to a KairosDB Database
    Can be created by giving a host value, a port value, a boolean representing whether https is used or http.

    This class have methods for :

    - Listing metric names from KairosDB
    - Querying data points from KairosDB for one or several metrics
    - Write one datapoint to any KairosDB metric
    - Write several datapoints to any KairosDB metrics existing or create a new metric with datapoints.
    - Get KairosDB server version
    - Get KairosDB server health status
    - Delete whole metric
    - Delete datapoints matching a query

    It aims to propose a method for every features avaiable at https://kairosdb.github.io/docs/build/html/restapi/index.html

    For the moment roll ups are not supported.
    """
    def __init__(self, host="localhost", port="8080", ssl=False):
        """
        Initialize a connection to a KairosDB database. Since the construction of an instance implies to get the server
        version through HTTP API, an instance can be returned only when KairosDB is responding.

        Parameters:
        ------------
          host  (str): Hostname of KairosDB server (IP Address or valid FQDN)
          port  (int): Port on which KairosDB server is listenning
          ssl   (bool): Use HTTPS instead of HTTP when communicating with KairosDB

        Returns:
        ------------
          con   (KairosClient): A connection object to the database
        """
        self.use_ssl = ssl
        self.host = host
        self.port = port
        self.__session__ = requests.Session()
        self.__generate_urls__()
        self.version = self.__get_server_version__()

    def __repr__(self):
        return "KairosClient(host={0}, port={1}, ssl={2})".format(
          self.host,
          self.port,
          self.use_ssl
        )
    
    def __generate_urls__(self):
        """
        This method creates all API URLs each time it is called. It does not return anything 
        and only set the value of self.urls
        """
        if self.use_ssl:
            self.protocol = "https"
        else:
            self.protocol = "http"
        self.base_url = "{0}://{1}:{2}/api/v1".format(self.protocol, self.host, self.port)
        self.urls = dict()
        self.urls["query"] = "{0}/datapoints/query".format(self.base_url)
        self.urls["query_tags"] = "{0}/datapoints/query/tags".format(self.base_url)
        self.urls["datapoints"] = "{0}/datapoints".format(self.base_url)
        self.urls["delete_datapoints"] = "{0}/datapoints/delete".format(self.base_url)
        self.urls["delete_metric"] = "{0}/metric".format(self.base_url)
        self.urls["health_check"] = "{0}/health/check".format(self.base_url)
        self.urls["health_status"] = "{0}/health/status".format(self.base_url)
        self.urls["version"] = "{0}/version".format(self.base_url)
        self.urls["metricnames"] = "{0}/metricnames".format(self.base_url)
        self.urls["tagnames"] = "{0}/tagnames".format(self.base_url)
        self.urls["metadata"] = "{0}/metadata".format(self.base_url)

    def __url__(self, endpoint):
        """
        Returns the URL for a given endpoint
        """
        try:
            return self.urls[endpoint]
        except KeyError:
            raise EndpointDoesNotExist("The given endpoint ({0}) does not exist. Available endpoints: {1}".format(
                tuple(self.urls.keys())
            ))

    def __prepare_request__(self, method, endpoint, json_body=None, headers=None, var=None, data=None):
        """
        Prepare a request to be sent in a requests session.
        This functions returns a prepared request with method, json_body and url set.
        """
        url = self.__url__(endpoint)
        if var is not None:
            url = url + "/" + var
        prep_request =  requests.Request(method=method, url=url, json=json_body, headers=headers, data=data).prepare()
        return prep_request

    def __send_request__(self, request, return_json=True, return_key=None):
        """
        Send a request and return the response. Optionnally returns the JSON response, 
        and potentially only value of a specific key.
        """
        response = self.__session__.send(request)
        rc = response.status_code
        if 200 <= rc < 300:
            if return_json:
                json_response = response.json()
                if return_key is not None:
                    return rc, json_response[return_key]
                return rc, json_response
        return rc, response
    
    def __post__(self, endpoint, json_body=None, headers=None, return_json=True, return_key=None, var=None, data=None):
        """
        Send a POST request to an endpoint
        """
        rqst = self.__prepare_request__("POST", endpoint=endpoint, json_body=json_body, headers=headers, var=var, data=data)
        return self.__send_request__(rqst, return_json=return_json, return_key=return_key)
    
    def __get__(self, endpoint, headers=None, return_json=True, return_key=None, var=None):
        """
        Send a GET request to an endpoint
        """
        rqst = self.__prepare_request__("GET", endpoint=endpoint, headers=headers, var=var)
        return self.__send_request__(rqst, return_json=return_json, return_key=return_key)
    
    def __delete__(self, endpoint, headers=None, var=None):
        """
        Send a DELETE request to an endpoint
        """
        rqst = self.__prepare_request__("DELETE", endpoint=endpoint, headers=headers, var=var)
        return self.__send_request__(rqst, return_json=False)
    
    def __get_server_version__(self):
        """
        Returns the KairosDB server version if successful. Fail if server cannot be reached or doesn't respond.
        """
        status_code, response = self.__get__("version")
        if status_code == 200:
            return response["version"]
        else:
            raise KairosDBError("An error occured. Status code was: {0} and error was: {1}".format(
                status_code,
                response.json()
            ))

    @staticmethod
    def __join_path_for_url__(*args):
        """
        Take the strings passed as parameters and joins them putting a '/' in order to
        form an endpoint of the metadata API. It validates and raises a ValueError if
        any of the parameters contains a '/' or '?' before joining
        """
        if any("/" in s or "?" in s for s in args):
            raise ValueError("Arguments must not contain '/' or '?' in order to create a valid url")
        return "/".join(args)

    def list_metrics(self, include_kairos_metrics=False):
        """
        List the available metrics from kairosDB database. 
        If include_kairos_metrics is set to True (Default), all metrics 
        including kairosdb own metrics are listed.
        If not the metrics created by user are listed.

        Parameters:
        ------------
            include_kairos_metrics (bool): Should all metrics be listed ? Default to True

        Returns:
        ------------
            metrics (list): List of available metrics
        """
        status_code, response = self.__get__("metricnames", return_json=True, return_key="results")
        if status_code == 200:
            if not include_kairos_metrics:
                return [metric_name for metric_name in response if "kairosdb." not in metric_name]
            else:
                return [metric_name for metric_name in response]
        else:
            raise KairosDBError("An error occured. Status code was: {0} and error was: {1}".format(
                status_code,
                response.json()
            ))

    def list_tagnames(self):
        """ List tag names """
        status_code, response = self.__get__("tagnames")
        if status_code == 200:
            return response["results"]
        else:
            raise KairosDBError("An error occured. Status code was: {0} and error was: {1}".format(
                status_code,
                response.json()
            ))

    def is_healthy(self):
        """
        Check the health of KairosDB server

        Returns:
        ----------
            is_healthy  (bool): True if KairosDB server is healthy, else False
        """
        status_code, response = self.__get__("health_check", return_json=False)
        if status_code == 204:
            return True
        else:
            return False

    def get_health_status(self):
        """
        Get the health status of KairosDB components
        """
        status_code, response = self.__get__("health_status", return_json=True)
        if status_code == 200:
            return response
        else:
            raise KairosDBError("An error occured. Status code was: {0} and error was: {1}".format(
                status_code,
                response.json()
            ))

    def query_metrics(self,
                      metrics,
                      start_absolute=None,
                      end_absolute=None,
                      start_relative=None,
                      end_relative=None,
                      tz="Etc/UTC",
                      cache_time=None,
                      to_pandas=True):
        """
        Query metrics.
        """
        query = Query(metrics,
                      start_absolute=start_absolute,
                      end_absolute=end_absolute,
                      start_relative=start_relative,
                      end_relative=end_relative,
                      tz=tz,
                      cache_time=cache_time
                     )
        
        status_code, response = self.__post__("query", json_body=query)
        if status_code == 200 and to_pandas:
            iterator = __iterate_on_query_response__(response)
            df = iterable_to_pandas(iterator)
            return df
        elif status_code == 200:
            return status_code, response
        else:
            raise KairosDBError("An error occured. Status code was: {0} and error was: {1}".format(
                status_code,
                response.json()
            ))
    
    def query(
        self,
        metric_names,
        start_absolute=None,
        start_relative=None,
        end_absolute=None,
        end_relative=None,
        tags=None,
        limit=None,
        aggregators=None,
        exclude_tags=False,
        group_by=None,
        order='asc',
        tz="Etc/UTC",
        cache_time=None,
        to_pandas=True):
        """
        Query metrics from KairosDB.

        Parameters:
        ------------
        metric_names    (list or str): List of metric names to query.
                                        If a string is given it will be transform into a list on length 1 holding the string.
                                        All other parameters will be applied to all metrics. If you want to query several metrics
                                        with different tags, use `query_metrics()` method instead.
        start_absolute  (str or datetime.datetime): Absolute start of the query. Must be given as a string representation of the date
                                                    or as datetime.datetime object. Default to None.
                                                    Either one of start_relative and start_absolute option must be given.
        start_relative  (tuple(int, str)): Relative start of the query.
        end_absolute    (str or datetime.datetime): Absolute end of the query.
        tags            (dict): A dictionaries of tags. Each tag consist in a pair of key and value. 
                                Value can be a single value, for example int, str, or boolean, or can be a list of values.
        limit           (int): Maximum number of point to return. Note that limit is applied before aggregation.
        aggregators     (list): A list of aggregators.
        exclude_tags    (bool): If set to True, the query does not return tag values of datapoints. 
                                Default to False, tags are returned.
        group_by        (??):
        order           (str): Either 'asc' or 'desc'. Order in which points are returned. Default to 'asc'.
        tz              (str): Valid long format timezone. Datapoints are always stored in Etc/UTC timezone. 
                                This option allows you to easily query using specific timezone.
                                Example: 'Europe/Paris'
        cache_time      (int): Number of seconds to keep the query in cache. By default queries are not cached.
        to_pandas       (bool): if set to True, query is returned as one or a list of pandas DataFrames 
                                    (in case group_by option was specified)

        Returns:
        ----------
        In case of failure:
            rc          (int): Returned status code
            response    (requests.models.Response): requests response
        In case of success:
            - if to_pandas is set to True:
                df      (pandas.DataFrame): 
            - if to_pandas is set to True and group_by is specified:
                dfs     (list of pandas.DataFrame)
            - if to_pandas is set to False:
                rc      (int): Returned status code
                resp    (requests.models.Response): requests response
        """
        # Always give metric_names as a list even if only one metric is being queried
        if is_string(metric_names):
            metric_names = [metric_names]
        # Create metric objects based on metric properties
        # Metric properties are shared between all metrics
        metrics = [
            Metric(metric_name, tags=tags, limit=limit, aggregators=aggregators, exclude_tags=exclude_tags, group_by=group_by, order=order)
            for metric_name 
            in metric_names
        ]

        return self.query_metrics(
            metrics,
            start_absolute=start_absolute,
            start_relative=start_relative,
            end_absolute=end_absolute,
            end_relative=end_relative,
            tz=tz,
            cache_time=cache_time,
            to_pandas=to_pandas
        )

    def __insert__(self, insert):
        """
        Send a message to datapoints endpoints in order to add datapoints.
        Accept any dictionary-like object that are valid insertion messages
        for KairosDB Rest API.
        """
        try:
            json_body = json.loads(insert.to_json())
        except AttributeError:
            json_body = insert
        return self.__post__("datapoints", json_body=json_body, return_json=False)
    
    def insert_point(self, name, value, timestamp, tags, ttl=None):
        """
        Insert a single point into KairosDB.
        value and timestamp are two non optional parameters.
        tags is also required: at least one tag should be added.

        Parameters:
        ------------
        name        (str): name of the metric in which the point will be added
        value       (numeric): Value of the point. Should be a numeric value
        timestamp   (int): Millisecond integer timestamp.
        tags        (dict): Dictionary of key values. Each value must be a single value
                            and not an iterable object. 
                            At least one tag must be set.
        ttl         (int): Cassandra ttl for the point. See cassandra doc on TTL for better understanding.
        """
        _point = Points(name, value=value, timestamp=timestamp, tags=tags, ttl=ttl)
        insert = Insert(_point)
        status_code, response  = self.__insert__(insert)
        if status_code == 204:
            return status_code, response
        else:
            raise KairosDBError("An error occured. Status code was: {0} and error was: {1}".format(
                status_code,
                response.json()
            ))

    def insert_datapoints(self, name, tags, datapoints=None, ttl=None):
        """
        Insert several datapoints at once.
        Datapoints must be a JSON serializable iterable holding element of length 2: [timestamp, value].
        At least one tag must be given.

        Parameters:
        ------------
        name        (str): name of the metric in which the point will be added
        datapoints  (iterable): Iterable holding datapoints.
                                Example:
                                [[timestamp1, value1], [timestamp2, value2], ... [timestampN, valueN]]
        tags        (dict): Dictionary of key values. Each value must be a single value
                            and not an iterable object. 
                            At least one tag must be set.
        ttl         (int): Cassandra ttl for the point. See cassandra doc on TTL for better understanding.
        """
        _points = Points(name, tags, datapoints=datapoints, ttl=ttl)
        insert = Insert(_points)
        status_code, response = self.__insert__(insert)
        if status_code == 204:
            return status_code, response
        else:
            raise KairosDBError("An error occured. Status code was: {0} and error was: {1}".format(
                status_code,
                response.json()
            ))
    
    def insert_dataframe(self, df, tag_columns=None, tags=None, timestamp_field="index", to_timestamp=None, ignore_columns=None):
        """
        Insert datapoints from a pandas DataFrame.
        You can:
        - specify some columns to be used at tags.
        - ignore some columns, I.E, not adding them as datapoints, and not using them as tags.
        - select which column or index should be used as timestamp index.
        - transform those values so that they become valid millisecond timestamps
        - add additional tags for all datapoints by giving a dictionary
        """
        insert = Insert.from_pandas(
            df,
            tag_columns=tag_columns,
            tags=tags,
            timestamp_field=timestamp_field,
            to_timestamp=to_timestamp,
            ignore_columns=ignore_columns
        )
        status_code, response = self.__insert__(insert)
        if status_code == 204:
            return status_code, response
        else:
            raise KairosDBError("An error occured. Status code was: {0} and error was: {1}".format(
                status_code,
                response.json()
            ))

    def delete_datapoints(
        self,
        metric_names,
        start_absolute=None,
        start_relative=None,
        end_absolute=None,
        end_relative=None,
        tags=None,
        limit=None,
        order='asc',
        tz="Etc/UTC"):
        """
        Delete will perform the query specified in the body and delete all data points returned by the query.
        Aggregators and groupers have no effect on which data points are deleted.
        Delete is designed such that you could perform a query,
        verify that the data points returned are correct, and issue the delete with that query.
        See documentation on `query()` method.

        Note: Delete works for the Cassandra and H2 data store only.
        """
        # Always give metric_names as a list even if only one metric is being queried
        if is_string(metric_names):
            metric_names = [metric_names]
        # Create metric objects based on metric properties
        # Metric properties are shared between all metrics
        metrics = [
            Metric(metric_name, tags=tags, limit=limit, order=order)
            for metric_name 
            in metric_names
        ]

        query = Query(
            metrics,
            start_absolute=start_absolute,
            end_absolute=end_absolute,
            start_relative=start_relative,
            end_relative=end_relative,
            tz=tz
        )
        
        status_code, response = self.__post__("delete_datapoints", json_body=query, return_json=False)
        if status_code == 204:
            return status_code, response
        else:
            raise KairosDBError("An error occured. Status code was: {0} and error was: {1}".format(
                status_code,
                response.json()
            ))

    def delete_metric(self, metric_name):
        """
        Deletes a metric and all data points associated with the metric.

        Note: Delete works for the Cassandra and H2 data stores only.
        """
        status_code, response = self.__delete__("delete_metric", var=metric_name)
        if status_code == 204:
            return status_code, response
        else:
            raise KairosDBError("An error occured. Status code was: {0} and error was: {1}".format(
                status_code,
                response.json()
            ))

    def query_tags(
        self,
        metric_names,
        start_absolute=None,
        start_relative=None,
        end_absolute=None,
        end_relative=None,
        tags=None,
        tz="Etc/UTC"):
        """
        Similar to a query but only returns the tags (no data points returned).
        This can potentially return more tags than a query because it is optimized
        for speed and does not query all rows to narrow down the time range. 
        This queries only the Row Key Index and thus the time range is the starting time range.
        Since the Cassandra row is set to 3 weeks, this can return tags for up to a 3 week period.
        """
        # Always give metric_names as a list even if only one metric is being queried
        if is_string(metric_names):
            metric_names = [metric_names]
        # Create metric objects based on metric properties
        # Metric properties are shared between all metrics
        metrics = [
            Metric(metric_name, tags=tags)
            for metric_name 
            in metric_names
        ]

        query = Query(
            metrics,
            start_absolute=start_absolute,
            end_absolute=end_absolute,
            start_relative=start_relative,
            end_relative=end_relative,
            tz=tz
        )
        
        status_code, response = self.__post__("query_tags", json_body=query)
        if status_code == 200:
            tags = list(__iterate_on_query_response__(response, return_tags=True))
            return tags
        else:
            raise KairosDBError("An error occured. Status code was: {0} and error was: {1}".format(
                status_code,
                response.json()
            ))

    def add_metadata_value(
        self,
        service,
        service_key,
        key,
        value):
        """
        Adds a value to service metadata or replaces it if there is one already.
        """
        key_route = self.__join_path_for_url__(service, service_key, key)
        status_code, response = self.__post__("metadata", data=str(value), var=key_route, return_json=False)
        if status_code == 204:
            return status_code, response
        else:
            raise KairosDBError("An error occured. Status code was: {0} and error was: {1}".format(
                status_code,
                response.json()
            ))

    def get_metadata_value(
        self,
        service,
        service_key,
        key):
        """
        Returns the value for the given service.
        """
        key_route = self.__join_path_for_url__(service, service_key, key)
        status_code, response = self.__get__("metadata", var=key_route, return_json=False)
        if status_code == 200:
            return response.content.decode()
        else:
            raise KairosDBError("An error occured. Status code was: {0} and error was: {1}".format(
                status_code,
                response.json()
            ))

    def list_metadata_service_keys(self, service):
        """
        Returns all keys for the given service or an empty list
        if no service keys exist for the given service.
        """
        key_route = self.__join_path_for_url__(service)
        status_code, response = self.__get__("metadata", var=key_route)
        if status_code == 200:
            return response["results"]
        else:
            raise KairosDBError("An error occured. Status code was: {0} and error was: {1}".format(
                status_code,
                response.json()
            ))

    def list_metadata_service_key_keys(self, service, service_key):
        """
        Returns all keys for the given service key or an empty list
        if no keys exist.
        """
        key_route = self.__join_path_for_url__(service, service_key)
        status_code, response = self.__get__("metadata", var=key_route)
        if status_code == 200:
            return response["results"]
        else:
            raise KairosDBError("An error occured. Status code was: {0} and error was: {1}".format(
                status_code,
                response.json()
            ))

    def delete_metadata_service_key_key(
        self,
        service,
        service_key,
        key):
        """
        Deletes the given key of a service key. Returns 204 as status code even
        when the key does not exist
        """
        key_route = self.__join_path_for_url__(service, service_key, key)
        status_code, response = self.__delete__("metadata", var=key_route)
        if status_code == 204:
            return status_code, response
        else:
            raise KairosDBError("An error occured. Status code was: {0} and error was: {1}".format(
                status_code,
                response.json()
            ))

    def get_all_metadata_values(
        self,
        service,
        service_key):
        """
        Returns a dictionary with all the key-value pairs for the given service key.
        """
        keys = self.list_metadata_service_key_keys(service, service_key)
        values = {}
        for key in keys:
            values[key] = self.get_metadata_value(service, service_key, key)
        return values