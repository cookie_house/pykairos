"""
------------------------------------------------
@author: Guillaume Charbonnier (Capgemini, 2018)
------------------------------------------------

"""
import json
import numpy
import datetime

class __MonsoonEncoder__(json.JSONEncoder):
    """A JSON encoder which can parser numpy types"""
    def default(self, obj):
        if isinstance(obj, numpy.integer):
            return int(obj)
        elif isinstance(obj, numpy.floating):
            return float(obj)
        elif isinstance(obj, numpy.ndarray):
            return obj.tolist()
        elif isinstance(obj, datetime.datetime):
            return obj.isoformat()
        else:
            return super(__MonsoonEncoder__, self).default(obj)


def to_strjson(obj, indent=None):
    """
    This function should be used the same way json.dumps is used.
    It allows to serialize datetime objects, which is not possible with standard library.
    
    ISO8601 format will be used, I.E, a string representing the date and time:
      YYYY-MM-DDTHH:MM:SS.mmmmmm 
    or, if microsecond is 0:
      YYYY-MM-DDTHH:MM:SS
    """
    return json.dumps(obj, indent=indent, cls=__MonsoonEncoder__)
