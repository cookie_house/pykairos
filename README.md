# Pykairos

> A Python client library to communicate with KairosDB

[![pipeline status](https://gitlab.com/cookie_house/pykairos/badges/master/pipeline.svg)](https://gitlab.com/cookie_house/pykairos/commits/master)
[![coverage report](https://gitlab.com/cookie_house/pykairos/badges/master/coverage.svg)](https://gitlab.com/cookie_house/pykairos/commits/master)


# Install from GIT

```bash
pip install git+https://gitlab.com/cookie_house/pykairos.git
```

# Developping pykairos

This library is packaged using [`poetry`](https://poetry.eustace.io) tool. 
If you want to install the library from source in order to do some development, run the following:

```bash
poetry install
```

If you wish to activate your virtual envionment simply run
```bash
poetry shell
```

Or if you wish to build a wheel for distribution:
```bash
poetry run
```

# Basic usage

```
from pykairos import KairosClient

db = KairosClient(host="localhost", port=8080, ssl=False)
db.list_metrics(include_kairos_metrics=False)
```

# Current state

- There is a folder named demo which allows to deploy KairosDB with Cassandra using Docker Swarm or Compose

- There is a doc folder but it is empty

- There is a notebook folder, it might be used to generate documentation automatically.

- There is a `tests` folder. Test coverage should be at least 80%

- There is `pykairos` directory which is the python package


# Next steps


- Improve code quality

- Improve tests

- Add ability to export/import data into KairosDB in a batch mode 

- Add ability to insert/query data in an asynchroneous fashion (not urgent at all)

- Add a CLI tool (even less urgent)

