SHELL:=/bin/bash

.PHONY: test

develop:
	poetry install

update:
	poetry update

develop-doc:
	poetry install -E docs

test:
	poetry run pytest -vv --cov-report term --cov=pykairos tests

lint:
	poetry run pylint pykairos/

build:
	poetry build

build-doc:
	poetry run mkdocs build

serve-doc:
	poetry run mkdocs serve

